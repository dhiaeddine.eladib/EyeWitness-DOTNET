﻿using EyeWitness.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Data.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private EyeWitnessContext dataContext;
        public EyeWitnessContext DataContext { get { return dataContext; } }

        public DatabaseFactory()
        {
            dataContext = new EyeWitnessContext();
        }
        protected override void DisposeCore()
        {
            if (DataContext != null)
                DataContext.Dispose();
        }
    }

}
