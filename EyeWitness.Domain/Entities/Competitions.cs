﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
   public class Competitions
    {
        public int Id { get; set; }
        public string theme { get; set; }
        public string descripiton { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public WitnessCard WitnessCard { get; set; }
    }
}
