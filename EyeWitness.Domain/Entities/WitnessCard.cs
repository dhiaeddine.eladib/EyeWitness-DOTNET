﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class WitnessCard
    {
        public int Id { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string picture { get; set; }
        public float note { get; set; }
        public string etat { get; set; }

        public ICollection<SubscriptionsWC> subscriptionsWC { get; set; }
        public ICollection<Evaluation> evaluations { get; set; }
        public User creator { get; set; }
        public User agent { get; set; }
        public ICollection<Competitions> competitions { get; set; }
        public ICollection<Events> events { get; set; }
        public Category category { get; set; }
    }
}
