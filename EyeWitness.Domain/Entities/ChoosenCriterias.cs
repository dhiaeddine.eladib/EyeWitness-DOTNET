﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class ChoosenCriterias
    {
        public int vote { get; set; }
        [Key, Column(Order = 0)]
        public int CategoryId { get; set; }
        public virtual Category category { get; set; }
        [Key, Column(Order = 1)]
        public int CriteriasId { get; set; }
        public virtual Criterias criterias { get; set; }
    }
}
