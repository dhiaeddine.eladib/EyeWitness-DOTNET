﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
   public class Events
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public string picture { get; set; }
        public WitnessCard witnessCard { get; set; }
    }
}
